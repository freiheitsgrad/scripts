# 2023-05-11,seimetz

Write-Host -ForegroundColor Yellow ""
Write-Host -ForegroundColor Yellow "############################################################################"
Write-Host -ForegroundColor Yellow "### This script temporarily mounts //home/asterix as the local drive Z:. ###"
Write-Host -ForegroundColor Yellow "###                                                                      ###"
Write-Host -ForegroundColor Yellow "### (Temporarily means in the scope of the system process, aka this      ###"
Write-Host -ForegroundColor Yellow "###  window. As soon as it is closed, the connection will be either.)    ###"                                                 ###"
Write-Host -ForegroundColor Yellow "############################################################################"
Write-Host -ForegroundColor Yellow ""

Write-Host -ForegroundColor Yellow " Checking if Z: is already in use..."
Write-Host -ForegroundColor Yellow ""

if ((Test-Path Z:))
{
Write-Host -ForegroundColor Yellow " ==> Drive Z: is already mounted, stopping here."
Write-Host -ForegroundColor Yellow ""
read-host "Press ENTER to close this window ..."
}
else
{
Write-Host -ForegroundColor Yellow " No, proceeding..."
Write-Host -ForegroundColor Yellow ""
Write-Host -ForegroundColor Yellow "	Enter your SMB-Crendentials in the pop-up window to mount //home/asterix as the local drive z:."
Write-Host -ForegroundColor Yellow " 		Note: Your SMB-Credentials might differ from your (standard) Asterix-Login."

$var = Get-Credential -Message "Enter (SMB) Username/Password"
# Although the -Persist parameter is set, the drive is only 'persistently' mapped in the scope of the system process, aka
# _as long as the window ist open_! Closing it unmaps the drive again.
New-PSDrive -Persist -Name "z" -Root "\\asterix.ieap.uni-kiel.de\home" -PSProvider "FileSystem" -Credential $var

Write-Host -ForegroundColor Yellow " Running 'Net Use' to check if process was successfull..."
Write-Host -ForegroundColor Yellow ""
Net Use

Write-Host -ForegroundColor Yellow "***"
Write-Host -ForegroundColor Yellow "*** If you want //home/asterix stay mounted, keep this window open."
Write-Host -ForegroundColor Yellow "***"
Write-Host -ForegroundColor Yellow ""
read-host "Pressing ENTER closes this window AND UNMOUNTS //home/asterix ..."
}