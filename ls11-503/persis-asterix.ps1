# 2023-05-11,seimetz

Write-Host -ForegroundColor Yellow ""
Write-Host -ForegroundColor Yellow "#############################################################################"
Write-Host -ForegroundColor Yellow "### This script persistently mounts //home/asterix as the local drive Z:. ###"
Write-Host -ForegroundColor Yellow "###                                                                       ###"
Write-Host -ForegroundColor Yellow "### (You will need to unmount it manually if necessary.)                  ###"                                                 ###"
Write-Host -ForegroundColor Yellow "#############################################################################"
Write-Host -ForegroundColor Yellow ""

Write-Host -ForegroundColor Yellow " Checking if Z: is already in use..."
Write-Host -ForegroundColor Yellow ""

if ((Test-Path Z:))
{
Write-Host -ForegroundColor Yellow " ==> Drive Z: is already mounted, stopping here."
Write-Host -ForegroundColor Yellow ""
read-host "Press ENTER to close this window ..."
}
else
{
Write-Host -ForegroundColor Yellow " No, proceeding..."
Write-Host -ForegroundColor Yellow ""
Write-Host -ForegroundColor Yellow "	Enter your SMB-Crendentials in the pop-up window to mount //home/asterix as the local drive z:."
Write-Host -ForegroundColor Yellow " 		Note: Your SMB-Credentials might differ from your (standard) Asterix-Login."

$var = Get-Credential -Message "Enter (SMB) Username/Password"
# The -Scope parameter set to 'Global' creates 'globally' persistent drives, which have to be unmapped manually.
New-PSDrive -Persist -Name "z" -Root "\\asterix.ieap.uni-kiel.de\home" -PSProvider "FileSystem" -Credential $var -Scope Global
Write-Host -ForegroundColor Yellow ""

Write-Host -ForegroundColor Yellow " Running 'Net Use' to check if process was successfull..."
Write-Host -ForegroundColor Yellow ""
Net Use

Write-Host -ForegroundColor Yellow ""
read-host "Done. Pressing ENTER closes this window ..."
}