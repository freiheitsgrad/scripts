# 2023-05-11,seimetz

Write-Host -ForegroundColor Yellow ""
Write-Host -ForegroundColor Yellow "###################################################################################"
Write-Host -ForegroundColor Yellow "### This script persistently mounts various Asterix folders as the local drives ###"
Write-Host -ForegroundColor Yellow "###                                                                             ###"        
Write-Host -ForegroundColor Yellow "###################################################################################"
Write-Host -ForegroundColor Yellow ""

Write-Host -ForegroundColor Yellow " Enter your SMB-Crendentials in the pop-up window."
Write-Host -ForegroundColor Yellow ""

$var = Get-Credential -Message "Enter (SMB) Username/Password"

##
## //home/asterix
##

Write-Host -ForegroundColor Yellow " Mounting //home/asterix to z:."
Write-Host -ForegroundColor Yellow " 	Checking if z: is already in use..."
Write-Host -ForegroundColor Yellow ""

if ((Test-Path Z:))
{
Write-Host -ForegroundColor Yellow " 	==> Drive z: is already mounted, stopping here."
Write-Host -ForegroundColor Yellow ""
read-host "Press ENTER to close this window ..."
}
else
{
Write-Host -ForegroundColor Yellow " 	No drive mounted as z:, good. Proceeding..."
# The -Scope parameter set to 'Global' creates 'globally' persistent drives, which have to be unmapped manually.
New-PSDrive -Persist -Name "z" -Root "\\asterix.ieap.uni-kiel.de\home" -PSProvider "FileSystem" -Credential $var -Scope Global
Write-Host -ForegroundColor Yellow " 	Done."
}

##
## //data/asterix/etph
##

Write-Host -ForegroundColor Yellow " Mounting //data/asterix/etph to y:."
Write-Host -ForegroundColor Yellow " 	Checking if y: is already in use..."
Write-Host -ForegroundColor Yellow ""

if ((Test-Path Y:))
{
Write-Host -ForegroundColor Yellow " 	==> Drive y: is already mounted, stopping here."
Write-Host -ForegroundColor Yellow ""
read-host "Press ENTER to close this window ..."
}
else
{
Write-Host -ForegroundColor Yellow " 	No drive mounted as y:, good. Proceeding..."
# The -Scope parameter set to 'Global' creates 'globally' persistent drives, which have to be unmapped manually.
New-PSDrive -Persist -Name "z" -Root "\\asterix.ieap.uni-kiel.de\etph" -PSProvider "FileSystem" -Credential $var -Scope Global
Write-Host -ForegroundColor Yellow " 	Done."
}

Write-Host -ForegroundColor Yellow " Running 'Net Use' to check if process was successfull..."
Write-Host -ForegroundColor Yellow ""
Net Use

Write-Host -ForegroundColor Yellow ""
read-host "Done. Pressing ENTER closes this window ..."
