# 2023-05-11,seimetz

Write-Host -ForegroundColor Yellow ""
Write-Host -ForegroundColor Yellow "######################################"
Write-Host -ForegroundColor Yellow "### This script unmounts drive Z:. ###"
Write-Host -ForegroundColor Yellow "######################################"
Write-Host -ForegroundColor Yellow ""

Write-Host -ForegroundColor Yellow " Checking if Z: is already in use..."
Write-Host -ForegroundColor Yellow ""

if ((Test-Path Z:))
{
Write-Host -ForegroundColor Yellow " ==> Drive Z: is mounted, unmounting."
Write-Host -ForegroundColor Yellow ""

$Drive = Get-WmiObject -Class Win32_mappedLogicalDisk -filter "ProviderName='\\\\asterix.ieap.uni-kiel.de\\home'"
net use $Drive.Name /delete /y

Write-Host -ForegroundColor Yellow "Done."
Write-Host -ForegroundColor Yellow ""
read-host "Press ENTER to close this window ..."
}
else
{
Write-Host -ForegroundColor Yellow " No drive Z: found, exiting."
Write-Host -ForegroundColor Yellow ""
read-host "Press ENTER to close this window ..."
}