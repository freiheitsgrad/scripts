# $Age = Read-Host "Please enter your age"
# $pwd_secure_string = Read-Host "Enter a Password" -AsSecureString

Write-Host -ForegroundColor Yellow " Enter your SMB-Crendentials to map //home/asterix as the local drive z:."
Write-Host -ForegroundColor Yellow ""
Write-Host -ForegroundColor Yellow " Note: Your SMB-Credentials might differ from your (standard) Asterix-Login."

$var = Get-Credential -Message "Enter (SMB) Username/Password"
New-PSDrive -Persist -Name "z" -Root "\\asterix.ieap.uni-kiel.de\home" -PSProvider "FileSystem" -Credential $var
Net Use
read-host "Press ENTER to continue..."